$("input").click(() => {
    let body = document.querySelector('body')
    if (body.classList.contains('morning')) {
        body.classList.remove('morning');
        body.classList.add('sunset');
        document.getElementById("greeting").innerHTML = "see u soon!";
    }
    else {
        body.classList.remove('sunset');
        body.classList.add('morning');
        document.getElementById("greeting").innerHTML = "hi, glad to see u again";
    }
});